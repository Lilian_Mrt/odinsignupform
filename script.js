const plant = document.getElementById("plant");

const blackArea = document.querySelector(".blackarea");

blackArea.setAttribute("style",`width : ${plant.offsetWidth}px`);

const right = document.querySelector(".right");

//*Passwords do not match

const userPass = document.getElementById("user_password");
const userPassConfirm = document.getElementById("user_password_confirm");
const messPasword = document.getElementById("messagepassword");

const passwords = document.querySelectorAll("input[type='password']");
passwords.forEach((password) => {
    password.addEventListener("input",() =>{
        if (userPass.value == userPassConfirm.value){
            userPass.classList.remove("error");
            userPassConfirm.classList.remove("error");
             messPasword.textContent = "";
        } else {
            messPasword.textContent = "*Passwords do not match";
            userPass.classList.add("error");
            userPassConfirm.classList.add("error");
        }
        
    })
});